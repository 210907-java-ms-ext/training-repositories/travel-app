import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountryListComponent } from './components/country-list/country-list.component';
import { GetCountryFromUserComponent } from './components/get-country-from-user/get-country-from-user.component';

const routes: Routes = [
  {path: 'country-list', component: CountryListComponent},
  {path: 'country-form', component: GetCountryFromUserComponent},
  {path: '', redirectTo: '/country-list', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
