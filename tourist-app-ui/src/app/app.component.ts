import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  //template: '<h1>This is in-line templating</h1>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tourist-app-ui';
}
