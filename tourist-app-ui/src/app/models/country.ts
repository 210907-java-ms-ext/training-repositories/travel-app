export interface Country {

    countryId: number;
    countryName: string;
    abbreviation: string;

}
