import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../models/country';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  countryList: Country[]  = [{countryId: 1, countryName: 'Japan', abbreviation: 'JPN'},
  {countryId: 2, countryName: 'Egypt', abbreviation: 'EG'},
  {countryId: 3, countryName: 'Germany', abbreviation: 'DE'},
  {countryId: 4, countryName: 'New Zeland', abbreviation: 'NZ'},
  {countryId: 5, countryName: 'China', abbreviation: 'CN'},
  {countryId: 6, countryName: 'Poland', abbreviation: 'PL'},
  {countryId: 7, countryName: 'United States', abbreviation: 'USA'}];

  //Observable - from RxJS (library, reactive programming for JS)
  // follow pub/sub design pattern
  public getAllCountries(): Observable<Country[]> {
    //return this.countryList;
    return this.httpClient.get<Country[]>("http://localhost:8080/TouristApp/countries");
  }

  public addCountry(country:Country) {
    this.countryList.push(country);
  }

  constructor(private httpClient: HttpClient) { }
}
