import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCountryFromUserComponent } from './get-country-from-user.component';

describe('GetCountryFromUserComponent', () => {
  let component: GetCountryFromUserComponent;
  let fixture: ComponentFixture<GetCountryFromUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GetCountryFromUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCountryFromUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
