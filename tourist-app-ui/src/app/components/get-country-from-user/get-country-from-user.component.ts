import { Component, OnInit } from '@angular/core';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-get-country-from-user',
  templateUrl: './get-country-from-user.component.html',
  styleUrls: ['./get-country-from-user.component.css']
})
export class GetCountryFromUserComponent implements OnInit {

  countryId: number = 0;

  countryName: string = "";

  abbreviation: string = "";

  constructor(public countryService: CountryService) { }

  ngOnInit(): void {
  }

  addCountry() {
    console.log(`adding country: ${this.countryId}, ${this.countryName}, ${this.abbreviation}`);
    this.countryService.addCountry({countryId: this.countryId, 
      countryName: this.countryName, 
      abbreviation: this.abbreviation
    });
  }

}
