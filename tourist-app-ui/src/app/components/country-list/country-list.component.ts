import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/models/country';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css'],
  //providers: [CountryService] creates new instance of CountryService
})
export class CountryListComponent implements OnInit {

  constructor(private countryService: CountryService) { }

  countryListData: Country[] = new Array();
  

  ngOnInit(): void {

    //this.countryListData = this.countryService.getAllCountries();
    //this.countryService.getAllCountries().toPromise().then(
      //(result) => {this.countryListData = result}
    //)

    this.countryService.getAllCountries().subscribe(
      (result) => { this.countryListData = result }
    );

  }

}
