import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CountryComponent } from './components/country/country.component';
import { CountryListComponent } from './components/country-list/country-list.component';
import { CountryService } from './services/country.service';
import { GetCountryFromUserComponent } from './components/get-country-from-user/get-country-from-user.component';

@NgModule({
  declarations: [
    AppComponent,
    CountryComponent,
    CountryListComponent,
    GetCountryFromUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [HttpClient],// [CountryService], creates Module level CountryService
  bootstrap: [AppComponent]
})
export class AppModule { }
