create table country (

country_id serial primary key,
country_name varchar(50) not null unique,
abbreviation varchar(10) not null unique

);

create table tourist_location (

city_name varchar(100) not null,
country_id int references country,
location_name varchar(100) not null,
address varchar(100) not null,
location_id serial primary key

);

select * from country;
insert into country (country_name, abbreviation) values('United States', 'USA');

insert into country (country_name, abbreviation) values ('Germany', 'DE');
insert into country (country_name, abbreviation) values ('South Korea', 'KR');
insert into country (country_name, abbreviation) 
	values 
	('Japan', 'JPN'),
	('France', 'FR'),
	('Greece', 'GR'),
	('Jordan', 'JOR'),
	('Mexico','MX'),
	('China','CN'),
	('Canada','CAN'),
	('Thailand','TH'),
	('Argentina', 'ARG'),
	('Singapore', 'SG');

select * from tourist_location;

insert into tourist_location (city_name, country_id, location_name, address)
	values 
		('CHINA', 9, 'Shanhai Pass', ' 40.431908, 116.570374.'),
		('Freiburg im Breisgau',2,'Aussichtsturm Schlossberg', '79104'),
		('Shanghai', 9, 'Starbucks Coffee','189 Nanjing W Rd'),
		('Petra','7','Petra','30.3285� N, 35.4444� E'),
		('Buenos Aires',12,'Recoleta','Rodriguez Pena 1291, C1021 CABA, Argentina'),
		('Berlin', 2, 'Berlin Zoologische Garten', 'Hardenbergpl. 8, 10787'),
		('Teton County',1,'Grand Tetons National Park','43.7904� N, 110.6818� W'),
		('ATHENS', 6, 'Acropolis', 'Athens 105 58, Greece');

select * from country natural join tourist_location;

