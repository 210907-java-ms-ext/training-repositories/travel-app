function getLocations(e) {
   
    e.preventDefault();

    let countryName = document.querySelector("#inputCountryName").value;

    let xhr = new XMLHttpRequest();

    xhr.open("GET", "locations?countryName="+countryName);
    
    xhr.onreadystatechange = function() {

        if (xhr.readyState === 4) {
            let locationList = JSON.parse(xhr.responseText);
            console.log(locationList);
            appendLocationsToDom(locationList);
        }

    }

    xhr.send();

}

function appendLocationsToDom(locationList) {
    let locationTable = document.querySelector("#locationTable");

    for (let location of locationList) {
        let row = document.createElement("tr");
        let locationId = document.createElement("td");
        locationId.innerHTML = location.locationId;
        row.appendChild(locationId);
        let locationName = document.createElement("td");
        locationName.innerHTML = location.locationName;
        row.appendChild(locationName);
        let address = document.createElement("td");
        address.innerHTML = location.address;
        row.appendChild(address);
        let cityName = document.createElement("td");
        cityName.innerHTML = location.cityName;
        row.appendChild(cityName);
        let countryId = document.createElement("td");
        countryId.innerHTML = location.country.countryId;
        row.appendChild(countryId);
        let countryName = document.createElement("td");
        countryName.innerHTML = location.country.countryName;
        row.appendChild(countryName);
        let abbreviation = document.createElement("td");
        abbreviation.innerHTML = location.country.abbreviation;
        row.appendChild(abbreviation);
        locationTable.appendChild(row);
    }

}

window.onload = function() {
    let button = document.querySelector("button");
    button.addEventListener("click", getLocations);
}