package com.revature.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.revature.model.Country;
import com.revature.model.Location;
import com.revature.service.ConnectionService;

public class LocationDaoImpl implements LocationDao {

	private ConnectionService connectionService;
	
	@Override
	public List<Location> getLocationsByCountry(Country country) {
		connectionService = new ConnectionService();

		String sql = "select * from tourist_location where country_id = ?";

		try (Connection connection = connectionService.getConnection()) {

			PreparedStatement pstmt = connection.prepareStatement(sql);

			pstmt.setInt(1, country.getCountryId());

			ResultSet resultSet = pstmt.executeQuery();

			List<Location> locationList = new ArrayList<>();

			while (resultSet.next()) {
				Location location = new Location();
				location.setLocationId(resultSet.getInt("location_id"));
				location.setLocationName(resultSet.getString("location_name"));
				location.setAddress(resultSet.getString("address"));
				location.setCityName(resultSet.getString("city_name"));
				location.setCountry(country);
				locationList.add(location);
			}

			return locationList;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public Location getLocationById(int locationId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Location getLocationByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteLocation(Location location) {
		// TODO Auto-generated method stub
		return;
	}

	@Override
	public Location createLocation(Location location) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Location updateLocation(Location location) {
		// TODO Auto-generated method stub
		return null;
	}

}
