package com.revature.data;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.revature.model.Country;
import com.revature.service.SessionService;

public class CountryDaoHibernate implements CountryDao {

	private static SessionFactory sf = SessionService.getSessionFactory();
	
	@Override
	public Country getCountryByName(String countryName) {
		
		Session sess = sf.openSession();
		
		String hql = "from Country as c where c.countryName = :name";
		
		Query query = sess.createQuery(hql);
		
		query.setParameter("name", countryName);
		
		return (Country) query.list().get(0);
		
	}

	@Override
	public List<Country> getAllCountries() {
		
		Session sess = sf.openSession();
		
		Query query = sess.createQuery("from Country");
		
		return query.list();
	}

	@Override
	public Country createCountry(Country country) {

		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		int id = (int)sess.save(country);
		country.setCountryId(id);
		System.out.println("Inserted Country: " + country);
		tx.commit();
		sess.close();
		return country;
	}

}
