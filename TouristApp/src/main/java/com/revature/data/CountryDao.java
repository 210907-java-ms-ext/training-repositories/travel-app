package com.revature.data;

import java.util.List;

import com.revature.model.Country;

public interface CountryDao {

	public Country getCountryByName(String countryName);
	
	public List<Country> getAllCountries();
	
	public Country createCountry(Country country);
	
}
