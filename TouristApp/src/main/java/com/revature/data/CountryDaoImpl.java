package com.revature.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.revature.model.Country;
import com.revature.service.ConnectionService;

public class CountryDaoImpl implements CountryDao {

	private ConnectionService connectionService;
	
	public Country getCountryByName(String countryName) {
		
		connectionService = new ConnectionService();
		
		String sql = "select * from country where country_name = ?";
		
		try (Connection connection = connectionService.getConnection()){
			
			PreparedStatement pstmt = connection.prepareStatement(sql);
			
			pstmt.setString(1, countryName);
			
			ResultSet resultSet = pstmt.executeQuery();

			Country country = new Country();
			
			while(resultSet.next()) {
				country.setCountryName(resultSet.getString("country_name"));
				country.setCountryId(resultSet.getInt("country_id"));
				country.setAbbreviation(resultSet.getString("abbreviation"));
			}
			
			return country;
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public List<Country> getAllCountries() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Country createCountry(Country country) {
		// TODO Auto-generated method stub
		return null;
	}

}
