package com.revature.data;

import java.util.List;

import com.revature.model.Country;
import com.revature.model.Location;

public interface LocationDao {
	
	public List<Location> getLocationsByCountry(Country country);
	
	public Location getLocationById(int locationId);
	
	public Location getLocationByName(String name);
	
	public void deleteLocation(Location location);
	
	public Location createLocation(Location location);
	
	public Location updateLocation(Location location);

}
