package com.revature.data;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import com.revature.model.Country;
import com.revature.model.Location;
import com.revature.service.SessionService;

public class LocationDaoHibernate implements LocationDao {

	private static SessionFactory sf = SessionService.getSessionFactory();
	
	@Override
	public List<Location> getLocationsByCountry(Country country) {
		
		Session sess = sf.openSession();
		
		String hql = "from Location where country = :countryParam";
		
		Query query = sess.createQuery(hql);
		
		query.setParameter("countryParam", country);
		
		return query.list();
	}

	@Override
	public Location getLocationById(int locationId) {

		Session sess = sf.openSession();
		return (Location) sess.get(Location.class, locationId);
	}

	@Override
	public Location getLocationByName(String name) {

		Session sess = sf.openSession();
		Criteria crit = sess.createCriteria(Location.class)
				.add(Restrictions.eq("name", name));
		return (Location) crit.list().get(0);
	}

	@Override
	public void deleteLocation(Location location) {
		
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		tx.commit();
		sess.delete(location);
	}

	@Override
	public Location createLocation(Location location) {
		
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		sess.save(location);
		tx.commit();
		return location;
	}

	@Override
	public Location updateLocation(Location location) {

		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		sess.update(location);
		tx.commit();
		return location;
	}

}
