package com.revature.service;

import java.util.List;

import com.revature.data.CountryDao;
import com.revature.data.CountryDaoHibernate;
import com.revature.model.Country;

public class CountryServiceImpl implements CountryService{

	public CountryDao countryDao = new CountryDaoHibernate();
	
	@Override
	public List<Country> getAllCountries() {
		return countryDao.getAllCountries();
	}

	@Override
	public Country addCountry(Country country) {
		return countryDao.createCountry(country);
	}
	

}
