package com.revature.service;

import java.util.List;

import com.revature.model.Country;

public interface CountryService {
	
	public List<Country> getAllCountries();
	
	public Country addCountry(Country country);

}
