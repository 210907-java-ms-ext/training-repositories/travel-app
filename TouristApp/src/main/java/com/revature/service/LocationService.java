package com.revature.service;

import java.util.List;

import com.revature.model.Location;

public interface LocationService {

	public List<Location> lookUpLocationsByCountryName(String countryName);
	
}
