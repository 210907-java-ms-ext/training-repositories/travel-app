package com.revature.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionService {

	private Connection connection;

	public ConnectionService() {
		super();

		try {
			Class.forName("org.postgresql.Driver");

			String url = System.getenv("TravelDB_URL");
			String username = System.getenv("TravelDB_Username");
			String password = System.getenv("TravelDB_Password");
			
			System.out.println("TravelDB_URL: " + url + "TravelDB_Username" + username + "TravelDB_Password" + password);

			connection = DriverManager.getConnection(url, username, password);

			if (false) {
				connection.setSchema("tourist_app");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

}
