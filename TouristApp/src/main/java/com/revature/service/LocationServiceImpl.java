package com.revature.service;

import java.util.List;

import com.revature.data.CountryDao;
import com.revature.data.CountryDaoHibernate;
import com.revature.data.LocationDao;
import com.revature.data.LocationDaoHibernate;
import com.revature.model.Country;
import com.revature.model.Location;

public class LocationServiceImpl implements LocationService {

	private LocationDao locationDao = new LocationDaoHibernate();
	
	private CountryDao countryDao = new CountryDaoHibernate();
	
	@Override
	public List<Location> lookUpLocationsByCountryName(String countryName) {
		
		Country country = countryDao.getCountryByName(countryName);
		
		return locationDao.getLocationsByCountry(country);
		
	}

}
