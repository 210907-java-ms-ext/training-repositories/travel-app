package com.revature.service;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import com.revature.model.Country;
import com.revature.model.Location;

public class SessionService {
	
	private static SessionFactory sf;
	
	static {
		Configuration configuration = new Configuration()
				.addPackage("com.revature.data")
				.addAnnotatedClass(Location.class)
				.addAnnotatedClass(Country.class)
				.configure();
		String url = System.getenv("TravelDB_URL");
		String username = System.getenv("TravelDB_Username");
		String password = System.getenv("TravelDB_Password");
		configuration.setProperty("hibernate.connection.url", url);
		configuration.setProperty("hibernate.connection.username", username);
		configuration.setProperty("hibernate.connection.password", password);
		ServiceRegistry serviceRegistery = 
				new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
		
		/*
		 * Metadata metadata = new MetadataSources(serviceRegistery)
		 * .addAnnotatedClass(Location.class) .addAnnotatedClass(Country.class)
		 * .getMetadataBuilder() .build();
		 */
		
		sf = configuration.buildSessionFactory(serviceRegistery);
		
		
	}
	
	public static SessionFactory getSessionFactory() {
		return sf;
	}

}
