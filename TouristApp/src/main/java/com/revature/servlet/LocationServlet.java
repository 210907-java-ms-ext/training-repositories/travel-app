package com.revature.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Location;
import com.revature.service.LocationService;
import com.revature.service.LocationServiceImpl;

public class LocationServlet extends HttpServlet{

	private LocationService locationService = new LocationServiceImpl();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
		
		String countryName = req.getParameter("countryName");
		
		List<Location> locationList = locationService.lookUpLocationsByCountryName(countryName);
		
		ObjectMapper objectMapper = new ObjectMapper();
		
		String json = objectMapper.writeValueAsString(locationList);
		
		resp.getWriter().write(json);
		
		resp.setStatus(200);
		
	}
	
}
