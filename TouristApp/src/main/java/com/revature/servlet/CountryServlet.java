package com.revature.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Country;
import com.revature.service.CountryService;
import com.revature.service.CountryServiceImpl;

public class CountryServlet extends HttpServlet {
	
	private CountryService countryService = new CountryServiceImpl();
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws IOException, ServletException {
		
		String countryName = req.getParameter("countryName");
		String abbreviation = req.getParameter("abbreviation");
		
		Country country = new Country();
		
		country.setAbbreviation(abbreviation);
		country.setCountryName(countryName);
		
		countryService.addCountry(country);
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		List<Country> countryList = countryService.getAllCountries();
		ObjectMapper om = new ObjectMapper();
		
		resp.getWriter().write(om.writeValueAsString(countryList));
		
	}

}
